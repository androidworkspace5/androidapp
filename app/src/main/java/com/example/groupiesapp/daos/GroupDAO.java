package com.example.groupiesapp.daos;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.example.groupiesapp.models.Group;

import java.util.List;

import io.reactivex.rxjava3.core.Completable;
import io.reactivex.rxjava3.core.Flowable;

@Dao
public interface GroupDAO
{
	@Query("SELECT * FROM `group`")
	Flowable<List<Group>> getAll();

	@Query("DELETE FROM `group`")
	Completable deleteAll();

	@Insert
	Completable insert(Group group);

	@Insert
	Completable insertAll(Group... groups);

	@Delete
	Completable delete(Group group);
}
