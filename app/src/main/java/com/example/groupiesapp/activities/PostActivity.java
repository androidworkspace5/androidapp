package com.example.groupiesapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.groupiesapp.R;
import com.example.groupiesapp.fragments.FeedFragment;
import com.example.groupiesapp.fragments.PostFragment;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;

public class PostActivity extends AppCompatActivity implements ActivityFragmentCommunication
{
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_post);

		openPostFragment();
	}

	@Override
	public void openFirstFragment()
	{

	}

	@Override
	public void openSignUpFragment()
	{

	}

	@Override
	public void openLogInFragment()
	{

	}

	@Override
	public void openForgotPasswordFragment()
	{

	}

	@Override
	public void openChooseGroupsFragment()
	{

	}


	@Override
	public void openFeedFragment()
	{

	}

	@Override
	public void openProfileFragment()
	{

	}

	@Override
	public void openGroupsMenuFragment()
	{

	}

	@Override
	public void openPostFragment()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = PostFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Post, PostFragment.newInstance(), tag);

		replaceTransaction.addToBackStack(tag);
		replaceTransaction.commit();
	}

	@Override
	public void openLikesFragment(String postID)
	{

	}

	@Override
	public void openFeedActivity()
	{
		Intent myIntent = new Intent(this, FeedActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void openMainActivity()
	{
		Intent myIntent = new Intent(this, MainActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void openPostActivity()
	{
		Intent myIntent = new Intent(this, PostActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void hideKeyboard()
	{
		View view = this.getCurrentFocus();
		if (view == null)
			return;

		InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}

	@Override
	public void onBackPressed()
	{
		openFeedActivity();
	}
}
