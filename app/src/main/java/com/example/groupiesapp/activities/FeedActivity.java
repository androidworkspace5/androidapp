package com.example.groupiesapp.activities;

import android.app.AlarmManager;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.groupiesapp.R;
import com.example.groupiesapp.broadcast.MyReceiver;
import com.example.groupiesapp.fragments.FeedFragment;
import com.example.groupiesapp.fragments.GroupsMenuFragment;
import com.example.groupiesapp.fragments.LikesFragment;
import com.example.groupiesapp.fragments.ProfileFragment;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;
import com.example.groupiesapp.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class FeedActivity extends AppCompatActivity implements ActivityFragmentCommunication
{
	@Override
	@RequiresApi(api = Build.VERSION_CODES.O)
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_feed);

		createNotification();
		loadUserFromDatabase();

		openFeedFragment();
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void createNotificationChannel()
	{
		CharSequence name = "TestNotificationChannel";
		String description = "Test Notification Description";
		int importance = NotificationManager.IMPORTANCE_DEFAULT;
		NotificationChannel channel = new NotificationChannel("testNotification", name, importance);
		channel.setDescription(description);
	}

	@RequiresApi(api = Build.VERSION_CODES.O)
	private void createNotification()
	{
		createNotificationChannel();
		Intent intent = new Intent(FeedActivity.this, MyReceiver.class);
		PendingIntent pendingIntent = PendingIntent.getBroadcast(FeedActivity.this, 0, intent, 0);

		AlarmManager alarmManager = (AlarmManager) getSystemService(ALARM_SERVICE);

		long currentTime = System.currentTimeMillis();

		alarmManager.setRepeating(AlarmManager.RTC_WAKEUP, currentTime + 1000 * 10, 1000 * 60, pendingIntent);
	}

	private void loadUserFromDatabase()
	{
		FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(getString(R.string.databaseLink));

		firebaseDatabase.getReference("Users").child(Objects.requireNonNull(FirebaseAuth.getInstance()
				.getCurrentUser()).getUid()).get().addOnCompleteListener(task ->
		{
			if (task.isSuccessful())
			{
				User user = User.getInstance();

				user.setFullName((String) task.getResult().child("fullName").getValue());
				user.setUsername((String) task.getResult().child("username").getValue());
				user.setEmailAddress((String) task.getResult().child("emailAddress").getValue());
				user.setCity((String) task.getResult().child("city").getValue());
			}
			else
			{
				Log.e("firebase/database", "Could not retrieve user details from database.");
			}
		});
	}

	@Override
	public void openProfileFragment()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = ProfileFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Feed, ProfileFragment.newInstance(), tag);

		replaceTransaction.addToBackStack(tag);
		replaceTransaction.commit();
	}


	@Override
	public void openFirstFragment()
	{

	}

	@Override
	public void openSignUpFragment()
	{

	}

	@Override
	public void openLogInFragment()
	{

	}

	@Override
	public void openForgotPasswordFragment()
	{

	}

	@Override
	public void openChooseGroupsFragment()
	{

	}

	@Override
	public void openFeedFragment()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = FeedFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Feed, FeedFragment.newInstance(), tag);

		replaceTransaction.commit();
	}

	@Override
	public void openGroupsMenuFragment()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = GroupsMenuFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Feed, GroupsMenuFragment.newInstance(), tag);

		replaceTransaction.addToBackStack(tag);
		replaceTransaction.commit();
	}

	@Override
	public void openPostFragment()
	{

	}

	@Override
	public void openLikesFragment(String postID)
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = LikesFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Feed, LikesFragment.newInstance(postID), tag);

		replaceTransaction.addToBackStack(tag);
		replaceTransaction.commit();
	}

	@Override
	public void openFeedActivity()
	{
		Intent myIntent = new Intent(this, FeedActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void openMainActivity()
	{
		Intent myIntent = new Intent(this, MainActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void openPostActivity()
	{
		Intent myIntent = new Intent(this, PostActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void hideKeyboard()
	{
		View view = this.getCurrentFocus();
		if (view == null)
			return;

		InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
}
