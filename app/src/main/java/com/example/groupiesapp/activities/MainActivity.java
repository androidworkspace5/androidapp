package com.example.groupiesapp.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.groupiesapp.R;
import com.example.groupiesapp.fragments.ChooseGroupsFragment;
import com.example.groupiesapp.fragments.FirstFragment;
import com.example.groupiesapp.fragments.ForgotPasswordFragment;
import com.example.groupiesapp.fragments.LogInFragment;
import com.example.groupiesapp.fragments.SignUpFragment;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;

public class MainActivity extends AppCompatActivity implements ActivityFragmentCommunication
{

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);

		openFirstFragment();
	}

	@Override
	public void openFirstFragment()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = FirstFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Main, FirstFragment.newInstance(), tag);
		replaceTransaction.commit();
	}

	@Override
	public void openSignUpFragment()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = SignUpFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Main, SignUpFragment.newInstance(), tag);

		replaceTransaction.addToBackStack(tag);
		replaceTransaction.commit();
	}

	@Override
	public void openLogInFragment()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = LogInFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Main, LogInFragment.newInstance(), tag);

		replaceTransaction.addToBackStack(tag);
		replaceTransaction.commit();
	}

	@Override
	public void openForgotPasswordFragment()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = ForgotPasswordFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Main, ForgotPasswordFragment.newInstance(), tag);

		replaceTransaction.addToBackStack(tag);
		replaceTransaction.commit();
	}

	@Override
	public void openChooseGroupsFragment()
	{
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		String tag = ChooseGroupsFragment.class.getName();

		FragmentTransaction replaceTransaction = transaction.replace(R.id.frame_layout_Main, ChooseGroupsFragment.newInstance(), tag);

		replaceTransaction.addToBackStack(tag);
		replaceTransaction.commit();
	}

	@Override
	public void openFeedFragment()
	{

	}

	@Override
	public void openProfileFragment()
	{

	}

	@Override
	public void openGroupsMenuFragment()
	{

	}

	@Override
	public void openPostFragment()
	{

	}

	@Override
	public void openLikesFragment(String postID)
	{

	}

	@Override
	public void openFeedActivity()
	{
		Intent myIntent = new Intent(this, FeedActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void openMainActivity()
	{
		Intent myIntent = new Intent(this, MainActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void openPostActivity()
	{
		Intent myIntent = new Intent(this, PostActivity.class);
		startActivity(myIntent);
		finish();
	}

	@Override
	public void hideKeyboard()
	{
		View view = this.getCurrentFocus();
		if (view == null)
			return;

		InputMethodManager inputManager = (InputMethodManager) this.getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
	}
}