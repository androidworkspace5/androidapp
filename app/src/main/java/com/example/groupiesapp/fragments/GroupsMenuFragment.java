package com.example.groupiesapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.groupiesapp.R;
import com.example.groupiesapp.adapters.GroupsAdapter;
import com.example.groupiesapp.daos.GroupDAO;
import com.example.groupiesapp.database.AppDatabase;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;
import com.example.groupiesapp.interfaces.OnGroupItemClick;
import com.example.groupiesapp.models.Group;

import java.util.ArrayList;

public class GroupsMenuFragment extends Fragment
{
	private ActivityFragmentCommunication fragmentCommunication;

	ArrayList<Group> groups = new ArrayList<>();
	RecyclerView groupsList;

	GroupsAdapter groupsAdapter = new GroupsAdapter(groups, new OnGroupItemClick()
	{
		@Override
		public void onClick(Group group)
		{
			if (fragmentCommunication != null)
			{
				fragmentCommunication.openGroupsMenuFragment();
			}
		}
	});

	public static GroupsMenuFragment newInstance()
	{
		Bundle args = new Bundle();

		GroupsMenuFragment fragment = new GroupsMenuFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_groups_menu, container, false);

		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
		groupsList = (RecyclerView) view.findViewById(R.id.userGroups);
		groupsList.setLayoutManager(linearLayoutManager);

		groups.clear();

		AppDatabase db = AppDatabase.getInstance(requireContext());
		GroupDAO groupDAO = db.groupDAO();

		groupDAO.getAll().subscribe(responseGroups ->
		{
			groups.addAll(responseGroups);

			Handler mainHandler = new Handler(requireContext().getMainLooper());
			Runnable myRunnable = () -> groupsAdapter.notifyDataSetChanged();
			mainHandler.post(myRunnable);
		});

		groupsList.setAdapter(groupsAdapter);

		fragmentCommunication.hideKeyboard();

		return view;
	}

	@Override
	public void onAttach(@NonNull Context context)
	{
		super.onAttach(context);

		if (context instanceof ActivityFragmentCommunication)
		{
			fragmentCommunication = (ActivityFragmentCommunication) context;
		}
	}
}
