package com.example.groupiesapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.groupiesapp.R;
import com.example.groupiesapp.adapters.GroupsAdapter;
import com.example.groupiesapp.adapters.PostsAdapter;
import com.example.groupiesapp.daos.GroupDAO;
import com.example.groupiesapp.database.AppDatabase;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;
import com.example.groupiesapp.interfaces.OnGroupItemClick;
import com.example.groupiesapp.interfaces.OnPostItemClick;
import com.example.groupiesapp.models.Group;
import com.example.groupiesapp.models.Post;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class FeedFragment extends Fragment
{
	private ActivityFragmentCommunication fragmentCommunication;

	ArrayList<Post> posts = new ArrayList<>();
	RecyclerView postsList;

	PostsAdapter postsAdapter = new PostsAdapter(posts, new OnPostItemClick()
	{
		@Override
		public void onClick(Post post)
		{
			fragmentCommunication.openLikesFragment(post.getPostID());
		}
	});

	public static FeedFragment newInstance()
	{
		Bundle args = new Bundle();

		FeedFragment fragment = new FeedFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_feed, container, false);

		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
		postsList = (RecyclerView) view.findViewById(R.id.posts_list);
		postsList.setLayoutManager(linearLayoutManager);

		posts.clear();

		FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(getString(R.string.databaseLink));
		DatabaseReference databaseReference = firebaseDatabase.getReference("Posts");

		databaseReference.addValueEventListener(new ValueEventListener()
		{
			@Override
			public void onDataChange(@NonNull DataSnapshot snapshot)
			{
				posts.clear();
				for (DataSnapshot dataSnapshotChild : snapshot.getChildren())
				{
					Post post = dataSnapshotChild.getValue(Post.class);
					posts.add(post);
				}

				postsAdapter.notifyDataSetChanged();
			}

			@Override
			public void onCancelled(@NonNull DatabaseError error)
			{
				Log.e("firebase/database", "Posts retrieval has been canceled.");
			}
		});

		postsList.setAdapter(postsAdapter);

		fragmentCommunication.hideKeyboard();

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		Button go_to_profile = view.findViewById(R.id.button_go_to_profile);
		go_to_profile.setOnClickListener(listener -> fragmentCommunication.openProfileFragment());

		Button openGroupsButton = view.findViewById(R.id.button_groups_menu);
		openGroupsButton.setOnClickListener(v -> fragmentCommunication.openGroupsMenuFragment());

		Button postButton = view.findViewById(R.id.button_make_post);
		postButton.setOnClickListener(v -> fragmentCommunication.openPostActivity());
	}

	@Override
	public void onAttach(@NonNull Context context)
	{
		super.onAttach(context);

		if (context instanceof ActivityFragmentCommunication)
		{
			fragmentCommunication = (ActivityFragmentCommunication) context;
		}
	}
}