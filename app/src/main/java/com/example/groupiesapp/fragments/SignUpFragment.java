package com.example.groupiesapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.groupiesapp.R;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;
import com.example.groupiesapp.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class SignUpFragment extends Fragment
{
	private ActivityFragmentCommunication fragmentCommunication;

	private FirebaseAuth firebaseAuth;
	private FirebaseDatabase firebaseDatabase;

	private EditText editTextUsername;
	private EditText editTextEmailAddress;
	private EditText editTextPassword;
	private EditText editTextRepeatPassword;

	public static SignUpFragment newInstance()
	{
		Bundle args = new Bundle();

		SignUpFragment fragment = new SignUpFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_sign_up, container, false);

		firebaseAuth = FirebaseAuth.getInstance();
		firebaseDatabase = FirebaseDatabase.getInstance(getString(R.string.databaseLink));

		editTextUsername = view.findViewById(R.id.username_SignUp);
		editTextEmailAddress = view.findViewById(R.id.email_SignUp);
		editTextPassword = view.findViewById(R.id.password_SignUp);
		editTextRepeatPassword = view.findViewById(R.id.repeatPassword_SignUp);

		fragmentCommunication.hideKeyboard();

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		Button signUpButton = view.findViewById(R.id.buttonSignUp_SignUp);
		signUpButton.setOnClickListener(listener ->
		{
			fragmentCommunication.hideKeyboard();

			String username = editTextUsername.getText().toString();
			String emailAddress = editTextEmailAddress.getText().toString();
			String password = editTextPassword.getText().toString();
			String repeatPassword = editTextRepeatPassword.getText().toString();

			if (!ValidateCredentials(username, emailAddress, password, repeatPassword))
				return;

			firebaseAuth.createUserWithEmailAndPassword(emailAddress, password).addOnCompleteListener(authTask ->
			{
				if (authTask.isSuccessful())
				{
					User user = User.getInstance();
					user.setUsername(username);
					user.setEmailAddress(emailAddress);

					firebaseDatabase.getReference("Users").child(Objects.requireNonNull(FirebaseAuth.getInstance()
							.getCurrentUser()).getUid()).setValue(user).addOnCompleteListener(databaseTask ->
					{
						if (databaseTask.isSuccessful())
						{
							Toast.makeText(getContext(), "Successfully registered!", Toast.LENGTH_SHORT).show();
							fragmentCommunication.openChooseGroupsFragment();
						}
						else
						{
							Toast.makeText(getContext(), "Registration failed! Database error.", Toast.LENGTH_SHORT).show();
						}
					});
				}
				else
				{
					Toast.makeText(getContext(), "Registration failed! Authentication error.", Toast.LENGTH_SHORT).show();
				}
			});
		});
	}

	@Override
	public void onAttach(@NonNull Context context)
	{
		super.onAttach(context);

		if (context instanceof ActivityFragmentCommunication)
		{
			fragmentCommunication = (ActivityFragmentCommunication) context;
		}
	}

	private boolean ValidateCredentials(String username, String emailAddress, String password, String repeatPassword)
	{
		if (username.isEmpty())
		{
			editTextUsername.setError("Username is required!");
			editTextUsername.requestFocus();
			return false;
		}

		if (emailAddress.isEmpty())
		{
			editTextEmailAddress.setError("Email address is required!");
			editTextEmailAddress.requestFocus();
			return false;
		}

		if (password.isEmpty())
		{
			editTextPassword.setError("Password is required!");
			editTextPassword.requestFocus();
			return false;
		}

		if (password.length() < 6)
		{
			editTextPassword.setError("Password must have more than 6 characters!");
			editTextPassword.requestFocus();
			return false;
		}

		if (!password.equals(repeatPassword))
		{
			editTextRepeatPassword.setError("Passwords are not the same!");
			editTextRepeatPassword.requestFocus();
			return false;
		}

		return true;
	}
}
