package com.example.groupiesapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.groupiesapp.R;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;
import com.google.firebase.auth.FirebaseAuth;

public class FirstFragment extends Fragment
{
    private ActivityFragmentCommunication fragmentCommunication;

    public static FirstFragment newInstance()
    {
        Bundle args = new Bundle();

        FirstFragment fragment = new FirstFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_first, container, false);

        FirebaseAuth firebaseAuth = FirebaseAuth.getInstance();

        fragmentCommunication.hideKeyboard();

        if (firebaseAuth.getCurrentUser() != null)
        {
            fragmentCommunication.openFeedActivity();
        }

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        Button signUpButton = view.findViewById(R.id.signUpButton_First);
        signUpButton.setOnClickListener(listener -> fragmentCommunication.openSignUpFragment());

        Button logInButton = view.findViewById(R.id.logInButton_First);
        logInButton.setOnClickListener(listener -> fragmentCommunication.openLogInFragment());
    }

    @Override
    public void onAttach(@NonNull Context context)
    {
        super.onAttach(context);

        if (context instanceof ActivityFragmentCommunication)
        {
            fragmentCommunication = (ActivityFragmentCommunication) context;
        }
    }
}
