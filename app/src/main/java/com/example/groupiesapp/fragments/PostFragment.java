package com.example.groupiesapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.groupiesapp.R;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;
import com.example.groupiesapp.models.Post;
import com.example.groupiesapp.models.User;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class PostFragment extends Fragment
{
	private ActivityFragmentCommunication fragmentCommunication;

	EditText writeText;

	public static PostFragment newInstance()
	{
		Bundle args = new Bundle();

		PostFragment fragment = new PostFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_post, container, false);

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		writeText = view.findViewById(R.id.write_text);

		Button cancelButton = view.findViewById(R.id.button_cancel);
		cancelButton.setOnClickListener(v -> fragmentCommunication.openFeedActivity());

		Button postButton = view.findViewById(R.id.button_post);
		postButton.setOnClickListener(v -> ProcessPost(writeText.getText().toString()));
	}

	private void ProcessPost(String text)
	{
		if (!ValidateData(text))
		{
			return;
		}

		FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(getString(R.string.databaseLink));
		DatabaseReference databaseReference = firebaseDatabase.getReference("Posts");

		String postID = databaseReference.push().getKey();

		if (postID == null)
		{
			Log.e("firebase/database", "Retrieved a null Post ID from database.");
			return;
		}

		Date c = Calendar.getInstance().getTime();
		SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy", Locale.getDefault());
		String formattedDate = df.format(c);

		Post post = new Post(postID, User.getInstance().getUsername(), formattedDate, text, new ArrayList<>());

		databaseReference.child(postID)
				.setValue(post)
				.addOnCompleteListener(task ->
				{
					if (task.isSuccessful())
					{
						Toast.makeText(getContext(), "Posted!", Toast.LENGTH_SHORT).show();
					}
					else
					{
						Toast.makeText(getContext(), "Posting failed!", Toast.LENGTH_SHORT).show();
					}

					fragmentCommunication.openFeedActivity();
				});
	}

	private boolean ValidateData(String text)
	{
		if (text.isEmpty())
		{
			writeText.setError("Text can not be empty!");
			writeText.requestFocus();
			return false;
		}

		return true;
	}

	@Override
	public void onAttach(@NonNull Context context)
	{
		super.onAttach(context);

		if (context instanceof ActivityFragmentCommunication)
		{
			fragmentCommunication = (ActivityFragmentCommunication) context;
		}
	}
}
