package com.example.groupiesapp.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.canhub.cropper.CropImage;
import com.example.groupiesapp.R;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;
import com.example.groupiesapp.models.User;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileFragment extends Fragment
{
	private ActivityFragmentCommunication fragmentCommunication;

	private FirebaseAuth firebaseAuth;
	private FirebaseDatabase firebaseDatabase;
	private StorageReference firebaseStorageReference;

	private EditText editTextFullName;
	private EditText editTextCity;
	private CircleImageView imageViewProfilePicture;

	private ActivityResultLauncher<Intent> activityResultLauncher;

	public static ProfileFragment newInstance()
	{
		Bundle args = new Bundle();

		ProfileFragment fragment = new ProfileFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Override
	public void onCreate(@Nullable Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);

		activityResultLauncher = registerForActivityResult(
				new ActivityResultContracts.StartActivityForResult(),
				result ->
				{
					if (result.getResultCode() == Activity.RESULT_OK)
					{
						CropImage.ActivityResult cropImageResult = CropImage.getActivityResult(result.getData());
						Uri resultUri = Objects.requireNonNull(cropImageResult).getUriContent();
						imageViewProfilePicture.setImageURI(resultUri);

						firebaseStorageReference.child("Profile Pictures")
								.child(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid())
								.child("profile-picture").putFile(Objects.requireNonNull(resultUri));
					}
				});
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_profile, container, false);

		firebaseAuth = FirebaseAuth.getInstance();
		firebaseDatabase = FirebaseDatabase.getInstance(getString(R.string.databaseLink));
		firebaseStorageReference = FirebaseStorage.getInstance().getReference();

		editTextFullName = view.findViewById(R.id.fullName_Profile);
		editTextCity = view.findViewById(R.id.city_Profile);
		imageViewProfilePicture = view.findViewById(R.id.image_Profile);

		System.out.println(FirebaseAuth.getInstance()
				.getCurrentUser().getUid());

		firebaseDatabase.getReference("Users").child(Objects.requireNonNull(FirebaseAuth.getInstance()
				.getCurrentUser()).getUid()).get().addOnCompleteListener(task ->
		{
			if (task.isSuccessful())
			{
				String fullName = (String) task.getResult().child("fullName").getValue();
				String city = (String) task.getResult().child("city").getValue();

				User user = User.getInstance();
				user.setFullName(fullName);
				user.setCity(city);

				try
				{
					StorageReference pathReference = firebaseStorageReference.child("Profile Pictures")
							.child(Objects.requireNonNull(firebaseAuth.getCurrentUser()).getUid())
							.child("profile-picture");

					File tempFile = File.createTempFile("images", "jpg");
					pathReference.getFile(tempFile).addOnSuccessListener(taskSnapshot ->
							imageViewProfilePicture.setImageURI(Uri.fromFile(tempFile)))
							.addOnFailureListener(e ->
									Log.e("firebase/database", "Error getting data", e));
				}
				catch (IOException ioException)
				{
					Log.e("tempfile/create", "TempFile for image download could not be created.");
				}
				catch (NullPointerException nullPointerException)
				{
					Log.e("firebase/storage", "Trying to get profile picture, but the user does not exist.");
				}

				editTextFullName.setText(fullName);
				editTextCity.setText(city);
			}
			else
			{
				Log.i("firebase/database", "Could not retrieve user data.");
			}
		});

		fragmentCommunication.hideKeyboard();

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		Button saveButton = view.findViewById(R.id.buttonSave_Profile);
		saveButton.setOnClickListener(listener ->
		{
			fragmentCommunication.hideKeyboard();

			String fullName = editTextFullName.getText().toString();
			String city = editTextCity.getText().toString();

			if (!ValidateDetails(fullName, city))
				return;

			User user = User.getInstance();
			user.setFullName(fullName);
			user.setCity(city);

			firebaseDatabase.getReference("Users").child(Objects.requireNonNull(FirebaseAuth.getInstance()
					.getCurrentUser()).getUid()).setValue(user).addOnCompleteListener(task ->
			{
				if (task.isSuccessful())
				{
					Toast.makeText(getContext(), "Details saved!", Toast.LENGTH_SHORT).show();
				}
				else
				{
					Toast.makeText(getContext(), "Saving failed!", Toast.LENGTH_SHORT).show();
				}
			});
		});

		Button logoutButton = view.findViewById(R.id.buttonLogout_Profile);
		logoutButton.setOnClickListener(listener ->
		{
			fragmentCommunication.hideKeyboard();

			firebaseAuth.signOut();
			Toast.makeText(getContext(), "Logged out", Toast.LENGTH_SHORT).show();
			fragmentCommunication.openMainActivity();
		});

		CircleImageView profilePicture = view.findViewById(R.id.image_Profile);
		profilePicture.setOnClickListener(listener -> ChooseProfilePicture());
	}

	@Override
	public void onDestroy()
	{
		super.onDestroy();

		activityResultLauncher.unregister();
	}

	@Override
	public void onAttach(@NonNull Context context)
	{
		super.onAttach(context);

		if (context instanceof ActivityFragmentCommunication)
		{
			fragmentCommunication = (ActivityFragmentCommunication) context;
		}
	}

	public void ChooseProfilePicture()
	{
		Intent cropImageIntent = CropImage.activity().setAspectRatio(1, 1).getIntent(requireContext());
		activityResultLauncher.launch(cropImageIntent);
	}

	private boolean ValidateDetails(String fullName, String city)
	{
		if (fullName.isEmpty())
		{
			editTextFullName.setError("Full name is required!");
			editTextFullName.requestFocus();
			return false;
		}

		if (city.isEmpty())
		{
			editTextCity.setError("City is required!");
			editTextCity.requestFocus();
			return false;
		}

		return true;
	}
}
