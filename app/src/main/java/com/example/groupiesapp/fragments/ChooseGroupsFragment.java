package com.example.groupiesapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.Cache;
import com.android.volley.Network;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.BasicNetwork;
import com.android.volley.toolbox.DiskBasedCache;
import com.android.volley.toolbox.HurlStack;
import com.android.volley.toolbox.JsonArrayRequest;
import com.example.groupiesapp.R;
import com.example.groupiesapp.adapters.ChooseGroupsAdapter;
import com.example.groupiesapp.daos.GroupDAO;
import com.example.groupiesapp.database.AppDatabase;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;
import com.example.groupiesapp.models.ChooseGroup;
import com.example.groupiesapp.models.Group;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;

public class ChooseGroupsFragment extends Fragment
{
	private ActivityFragmentCommunication fragmentCommunication;

	private final String groupsURL = "https://my-json-server.typicode.com/AndreyLumineux/GroupiesDB/groups";

	ArrayList<ChooseGroup> groupsArrayList = new ArrayList<>();
	RecyclerView groupsRecyclerView;

	ChooseGroupsAdapter adapter = new ChooseGroupsAdapter(groupsArrayList);

	public static ChooseGroupsFragment newInstance()
	{
		Bundle args = new Bundle();

		ChooseGroupsFragment fragment = new ChooseGroupsFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_choose_groups, container, false);

		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
		groupsRecyclerView = view.findViewById(R.id.groups_list);
		groupsRecyclerView.setLayoutManager(linearLayoutManager);

		groupsArrayList.clear();

		RequestQueue requestQueue;

		Cache cache = new DiskBasedCache(requireContext().getCacheDir(), 1024 * 1024);
		Network network = new BasicNetwork(new HurlStack());
		requestQueue = new RequestQueue(cache, network);
		requestQueue.start();

		Type arrayListOfGroupsType = new TypeToken<ArrayList<ChooseGroup>>()
		{
		}.getType();

		JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, groupsURL, null, response ->
		{
			groupsArrayList.addAll(new Gson().fromJson(String.valueOf(response), arrayListOfGroupsType));
			groupsRecyclerView.setAdapter(adapter);
		}, error ->
		{
			Log.e("groups/http", "Could not retrieve & deserialize the groups response.", error);
			fragmentCommunication.openFeedActivity();
		});

		requestQueue.add(jsonArrayRequest);

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		AppDatabase db = AppDatabase.getInstance(requireContext());
		GroupDAO groupDAO = db.groupDAO();

		new Thread(() -> groupDAO.deleteAll().subscribe()).start();

		Button joinButton = view.findViewById(R.id.button_join);
		joinButton.setOnClickListener(v ->
		{
			for (int index = 0; index < groupsRecyclerView.getChildCount(); index++)
			{
				CheckBox checkBox = groupsRecyclerView.getChildAt(index).findViewById(R.id.choose_group);

				groupsArrayList.get(index).setChecked(checkBox.isChecked());

				if (checkBox.isChecked())
				{
					Group group = new Group(groupsArrayList.get(index).getGroupName());
					new Thread(() -> groupDAO.insertAll(group).subscribe()).start();
				}
			}

			fragmentCommunication.openFeedActivity();
		});
	}

	@Override
	public void onAttach(@NonNull Context context)
	{
		super.onAttach(context);

		if (context instanceof ActivityFragmentCommunication)
		{
			fragmentCommunication = (ActivityFragmentCommunication) context;
		}
	}
}
