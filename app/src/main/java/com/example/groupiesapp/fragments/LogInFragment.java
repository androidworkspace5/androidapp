package com.example.groupiesapp.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.fragment.app.Fragment;

import com.example.groupiesapp.R;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;
import com.example.groupiesapp.models.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Objects;

public class LogInFragment extends Fragment
{
	private ActivityFragmentCommunication fragmentCommunication;

	private FirebaseAuth firebaseAuth;

	private EditText editTextEmail;
	private EditText editTextPassword;

	public static LogInFragment newInstance()
	{
		Bundle args = new Bundle();

		LogInFragment fragment = new LogInFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_log_in, container, false);

		fragmentCommunication.hideKeyboard();

		editTextEmail = view.findViewById(R.id.email_LogIn);
		editTextPassword = view.findViewById(R.id.password_LogIn);

		SharedPreferences sharedPreferences = requireContext().getSharedPreferences("mainPrefs", Context.MODE_PRIVATE);

		String email = sharedPreferences.getString("Email", "");
		String password = sharedPreferences.getString("Password", "");

		editTextEmail.setText(email);
		editTextPassword.setText(password);

		firebaseAuth = FirebaseAuth.getInstance();

		if (firebaseAuth.getCurrentUser() != null)
		{
			fragmentCommunication.openProfileFragment();
		}

		return view;
	}

	@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
	void save(String key, EditText text)
	{
		SharedPreferences sharedPreferences = requireContext().getSharedPreferences("mainPrefs", Context.MODE_PRIVATE);
		SharedPreferences.Editor myEdit = sharedPreferences.edit();
		myEdit.putString(key, text.getText().toString());

		myEdit.apply();
	}

	@RequiresApi(api = Build.VERSION_CODES.GINGERBREAD)
	@Override
	public void onPause()
	{
		super.onPause();

		save("Email", editTextEmail);
		save("Password", editTextPassword);
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		Button logInButton = view.findViewById(R.id.buttonLogIn_LogIn);
		logInButton.setOnClickListener(listener ->
		{
			fragmentCommunication.hideKeyboard();

			String emailAddress = editTextEmail.getText().toString();
			String password = editTextPassword.getText().toString();

			if (!ValidateDetails(emailAddress, password))
				return;

			firebaseAuth.signInWithEmailAndPassword(emailAddress, password).addOnCompleteListener(task ->
			{
				if (task.isSuccessful())
				{
					Toast.makeText(requireContext(), "Logged in!", Toast.LENGTH_SHORT).show();
					fragmentCommunication.openFeedActivity();
				}
				else
				{
					Toast.makeText(requireContext(), "Login failed!", Toast.LENGTH_SHORT).show();
				}
			});
		});

		Button forgotPasswordButton = view.findViewById(R.id.button_forgotPassword_LogIn);
		forgotPasswordButton.setOnClickListener(listener -> fragmentCommunication.openForgotPasswordFragment());
	}

	@Override
	public void onAttach(@NonNull Context context)
	{
		super.onAttach(context);

		if (context instanceof ActivityFragmentCommunication)
		{
			fragmentCommunication = (ActivityFragmentCommunication) context;
		}
	}

	private boolean ValidateDetails(String emailAddress, String password)
	{
		if (emailAddress.isEmpty())
		{
			editTextEmail.setError("Email address is required!");
			editTextEmail.requestFocus();
			return false;
		}

		if (password.isEmpty())
		{
			editTextPassword.setError("Password is required!");
			editTextPassword.requestFocus();
			return false;
		}

		return true;
	}
}
