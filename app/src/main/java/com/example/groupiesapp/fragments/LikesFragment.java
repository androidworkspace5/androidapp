package com.example.groupiesapp.fragments;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.groupiesapp.R;
import com.example.groupiesapp.adapters.LikesAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class LikesFragment extends Fragment
{
	ArrayList<String> likes = new ArrayList<>();
	RecyclerView likesList;

	LikesAdapter likesAdapter = new LikesAdapter(likes);

	public static LikesFragment newInstance(String postID)
	{
		Bundle args = new Bundle();
		args.putCharSequence("postID", postID);

		LikesFragment fragment = new LikesFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_likes, container, false);

		LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
		likesList = (RecyclerView) view.findViewById(R.id.likes_list);
		likesList.setLayoutManager(linearLayoutManager);

		likes.clear();

		if (getArguments() != null)
		{
			String postID = (String) getArguments().getCharSequence("postID");

			FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(getString(R.string.databaseLink));
			DatabaseReference databaseReference = firebaseDatabase.getReference("Posts").child(postID).child("likes");

			databaseReference.addValueEventListener(new ValueEventListener()
			{
				@Override
				public void onDataChange(@NonNull DataSnapshot snapshot)
				{
					likes.clear();

					for (DataSnapshot dataSnapshotChild : snapshot.getChildren())
					{
						String like = dataSnapshotChild.getValue(String.class);
						likes.add(like);
					}

					likesAdapter.notifyDataSetChanged();
				}

				@Override
				public void onCancelled(@NonNull DatabaseError error)
				{
					Log.e("firebase/database", "Posts retrieval has been canceled.");
				}
			});

			likesList.setAdapter(likesAdapter);
		}

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);
	}
}
