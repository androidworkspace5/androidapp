package com.example.groupiesapp.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.groupiesapp.R;
import com.example.groupiesapp.interfaces.ActivityFragmentCommunication;

public class ForgotPasswordFragment extends Fragment
{
	private ActivityFragmentCommunication fragmentCommunication;

	public static ForgotPasswordFragment newInstance()
	{
		Bundle args = new Bundle();

		ForgotPasswordFragment fragment = new ForgotPasswordFragment();
		fragment.setArguments(args);
		return fragment;
	}

	@Nullable
	@Override
	public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState)
	{
		View view = inflater.inflate(R.layout.fragment_forgot_password, container, false);

		return view;
	}

	@Override
	public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState)
	{
		super.onViewCreated(view, savedInstanceState);

		Button submitButton = view.findViewById(R.id.button_submit);
		submitButton.setOnClickListener(listener ->
		{
			//TODO submit button function
		});
	}

	@Override
	public void onAttach(@NonNull Context context)
	{
		super.onAttach(context);

		if (context instanceof ActivityFragmentCommunication)
		{
			fragmentCommunication = (ActivityFragmentCommunication) context;
		}
	}
}
