package com.example.groupiesapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.groupiesapp.models.Group;
import com.example.groupiesapp.R;
import com.example.groupiesapp.interfaces.OnGroupItemClick;

import java.util.ArrayList;

public class GroupsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	ArrayList<Group> groups;
	private OnGroupItemClick onGroupItemClick;

	public GroupsAdapter(ArrayList<Group> groups, OnGroupItemClick onGroupItemClick)
	{
		this.groups = groups;
		this.onGroupItemClick = onGroupItemClick;
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View view = layoutInflater.inflate(R.layout.groups_item_cell, parent, false);
		GroupsViewHolder groupsViewHolder = new GroupsViewHolder(view);
		return groupsViewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
	{
		if (holder instanceof GroupsViewHolder)
		{
			Group group = (Group) groups.get(position);
			((GroupsViewHolder) holder).bind(group);
		}
	}

	@Override
	public int getItemCount()
	{
		return this.groups.size();
	}

	class GroupsViewHolder extends RecyclerView.ViewHolder
	{
		private TextView name;
		View view;

		GroupsViewHolder(View view)
		{
			super(view);
			name = view.findViewById(R.id.group_name);
			this.view = view;
		}

		void bind(Group group)
		{
			name.setText(group.getGroupName());
			view.setOnClickListener(v ->
			{
				if (onGroupItemClick != null)
				{
					onGroupItemClick.onClick(group);
				}
			});
		}
	}
}

