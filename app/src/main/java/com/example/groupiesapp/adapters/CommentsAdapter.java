package com.example.groupiesapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.groupiesapp.R;
import com.example.groupiesapp.models.Comment;

import java.util.ArrayList;

public class CommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	ArrayList<Comment> comments;

	public CommentsAdapter(ArrayList<Comment> comments)
	{
		this.comments = comments;
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View view = layoutInflater.inflate(R.layout.comments_item_cell, parent, false);
		return new CommentsViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
	{
		if (holder instanceof CommentsAdapter.CommentsViewHolder)
		{
			Comment comment = comments.get(position);
			((CommentsAdapter.CommentsViewHolder) holder).bind(comment);
		}
	}

	@Override
	public int getItemCount()
	{
		return this.comments.size();
	}

	class CommentsViewHolder extends RecyclerView.ViewHolder
	{
		//private image
		private final TextView userName;
		private final TextView date;
		private final TextView commentText;
		View view;

		CommentsViewHolder(View view)
		{
			super(view);
			userName = view.findViewById(R.id.user_who_comment_post);
			date = view.findViewById(R.id.comment_date);
			commentText = view.findViewById(R.id.text_comment);
			this.view = view;
		}

		void bind(Comment comment)
		{
			userName.setText(comment.getName());
			date.setText((CharSequence) comment.getDate());
			commentText.setText(comment.getComment());
		}
	}
}
