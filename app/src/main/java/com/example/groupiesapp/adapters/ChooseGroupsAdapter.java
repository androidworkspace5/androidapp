package com.example.groupiesapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.groupiesapp.R;
import com.example.groupiesapp.models.ChooseGroup;

import java.util.ArrayList;

public class ChooseGroupsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	ArrayList<ChooseGroup> groups;

	public ChooseGroupsAdapter(ArrayList<ChooseGroup> groups)
	{
		this.groups = groups;
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View view = layoutInflater.inflate(R.layout.check_group_item_cell, parent, false);
		ChooseGroupsAdapter.ChooseGroupsViewHolder chooseGroupsViewHolder = new ChooseGroupsAdapter.ChooseGroupsViewHolder(view);
		return chooseGroupsViewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
	{
		if (holder instanceof ChooseGroupsViewHolder)
		{
			ChooseGroup group = (ChooseGroup) groups.get(position);
			((ChooseGroupsAdapter.ChooseGroupsViewHolder) holder).bind(group);
		}
	}

	@Override
	public int getItemCount()
	{
		return this.groups.size();
	}

	class ChooseGroupsViewHolder extends RecyclerView.ViewHolder
	{
		private String groupName;
		private CheckBox checkBox;
		View view;

		ChooseGroupsViewHolder(View view)
		{
			super(view);
			checkBox = view.findViewById(R.id.choose_group);
			groupName = (String) checkBox.getText();
			this.view = view;
		}

		void bind(ChooseGroup group)
		{
			groupName = group.getGroupName();
			checkBox.setText(groupName);
			//view.setOnClickListener(new View.OnClickListener(){});
		}
	}
}
