package com.example.groupiesapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.groupiesapp.R;

import java.util.ArrayList;

public class LikesAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	//private image
	private String userName;

	ArrayList<String> likes;

	public LikesAdapter(ArrayList<String> likes)
	{
		this.likes = likes;
	}

	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View view = layoutInflater.inflate(R.layout.likes_item_cell, parent, false);
		LikesViewHolder likesViewHolder = new LikesViewHolder(view);
		return likesViewHolder;
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
	{
		if (holder instanceof LikesAdapter.LikesViewHolder)
		{
			String like = (String) likes.get(position);
			((LikesAdapter.LikesViewHolder) holder).bind(like);
		}
	}

	@Override
	public int getItemCount()
	{
		return likes.size();
	}

	class LikesViewHolder extends RecyclerView.ViewHolder
	{
		private TextView userName;
		View view;

		LikesViewHolder(View view)
		{
			super(view);
			userName = view.findViewById(R.id.user_who_liked_post);
			this.view = view;
		}

		void bind(String like)
		{
			userName.setText(like);
		}
	}
}
