package com.example.groupiesapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.groupiesapp.R;
import com.example.groupiesapp.interfaces.OnPostItemClick;
import com.example.groupiesapp.models.Post;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;

public class PostsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>
{
	ArrayList<Post> posts;
	private final OnPostItemClick onPostItemClick;

	public PostsAdapter(ArrayList<Post> posts, OnPostItemClick onPostItemClick)
	{
		this.posts = posts;
		this.onPostItemClick = onPostItemClick;
	}


	@NonNull
	@Override
	public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
	{
		LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
		View view = layoutInflater.inflate(R.layout.posts_item_cell, parent, false);
		return new PostsViewHolder(view);
	}

	@Override
	public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position)
	{
		if (holder instanceof PostsAdapter.PostsViewHolder)
		{
			Post post = posts.get(position);
			((PostsAdapter.PostsViewHolder) holder).bind(post);
		}
	}

	@Override
	public int getItemCount()
	{
		return this.posts.size();
	}

	class PostsViewHolder extends RecyclerView.ViewHolder
	{
		private final TextView userName;
		private final TextView date;
		private final TextView text;
		private final TextView likesNumber;
		private final TextView commentsNumber;

		private final Button likeButton;
		View view;

		PostsViewHolder(View view)
		{
			super(view);
			userName = view.findViewById(R.id.text_user_name);
			date = view.findViewById(R.id.text_date);
			text = view.findViewById(R.id.text_post);
			likesNumber = view.findViewById(R.id.text_likes_numbers);
			commentsNumber = view.findViewById(R.id.text_comments_numbers);

			likeButton = view.findViewById(R.id.button_like);

			this.view = view;
		}

		void bind(Post post)
		{
			String userNameText = "Posted by: " + post.getUserName();
			userName.setText(userNameText);

			String dateText = "Posted on: " + (CharSequence) post.getDate();
			date.setText(dateText);

			text.setText(post.getText());

			String likesText = "Likes: " + post.getLikesNumber();
			likesNumber.setText(likesText);

			String commentsText = "Comments: " + post.getCommentsNumber();
			commentsNumber.setText(commentsText);

			view.setOnClickListener(v ->
			{
				if (onPostItemClick != null)
				{
					onPostItemClick.onClick(post);
				}
			});

			likeButton.setOnClickListener(v ->
			{
				post.LikePressed();

				FirebaseDatabase firebaseDatabase = FirebaseDatabase.getInstance(view.getContext().getString(R.string.databaseLink));
				firebaseDatabase.getReference().child("Posts").child(post.getPostID()).child("likes").setValue(post.getLikes());

				notifyDataSetChanged();
			});
		}
	}
}
