package com.example.groupiesapp.interfaces;

public interface ActivityFragmentCommunication
{
	void openFirstFragment();

	void openSignUpFragment();

	void openLogInFragment();

	void openForgotPasswordFragment();

	void openChooseGroupsFragment();

	void openFeedFragment();

	void openProfileFragment();

	void openGroupsMenuFragment();

	void openPostFragment();

	void openLikesFragment(String postID);

	void openFeedActivity();

	void openMainActivity();

	void openPostActivity();

	void hideKeyboard();
}
