package com.example.groupiesapp.interfaces;

import com.example.groupiesapp.models.Group;

public interface OnGroupItemClick
{
	void onClick(Group group);
}

