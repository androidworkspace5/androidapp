package com.example.groupiesapp.interfaces;

import com.example.groupiesapp.models.Post;

public interface OnPostItemClick
{
	void onClick(Post post);
}
