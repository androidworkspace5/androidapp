package com.example.groupiesapp.models;

public class ChooseGroup
{
	private String groupName;
	private Boolean isChecked;

	public ChooseGroup(String name, Boolean isChecked)
	{
		this.groupName = name;
		this.isChecked = isChecked;
	}

	public String getGroupName()
	{
		return groupName;
	}

	public Boolean getChecked()
	{
		return isChecked;
	}

	public void setChecked(Boolean checked)
	{
		isChecked = checked;
	}

	public void setGroupName(String groupName)
	{
		this.groupName = groupName;
	}
}
