package com.example.groupiesapp.models;

import java.util.ArrayList;

public class Post
{
	private String postID;
	private String userName;
	private String date;
	private String text;
	private int commentsNumber;
	private ArrayList<String> likes = new ArrayList<>();

	public Post()
	{

	}

	public Post(String postID, String userName, String date, String text, ArrayList<String> likes)
	{
		this.postID = postID;
		this.userName = userName;
		this.date = date;
		this.text = text;
		this.likes = likes;
	}

	public void LikePressed()
	{
		String username = User.getInstance().getUsername();

		if (likes.contains(username))
		{
			likes.remove(username);
		}
		else
		{
			likes.add(username);
		}
	}

	public String getPostID()
	{
		return postID;
	}

	public void setPostID(String postID)
	{
		this.postID = postID;
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName)
	{
		this.userName = userName;
	}

	public String getDate()
	{
		return date;
	}

	public void setDate(String date)
	{
		this.date = date;
	}

	public String getText()
	{
		return text;
	}

	public void setText(String text)
	{
		this.text = text;
	}

	public int getLikesNumber()
	{
		return likes.size();
	}

	public int getCommentsNumber()
	{
		return commentsNumber;
	}

	public void setCommentsNumber(int commentsNumber)
	{
		this.commentsNumber = commentsNumber;
	}

	public ArrayList<String> getLikes()
	{
		return likes;
	}

	public void setLikes(ArrayList<String> likes)
	{
		this.likes = likes;
	}
}
