package com.example.groupiesapp.models;

public class User
{
	private static User instance;

	private String fullName;
	private String username;
	private String emailAddress;
	private String city;

	public User()
	{

	}

	public static User getInstance()
	{
		if (instance == null)
		{
			instance = new User();
		}

		return instance;
	}

	public String getFullName()
	{
		return fullName;
	}

	public void setFullName(String fullName)
	{
		this.fullName = fullName;
	}

	public String getUsername()
	{
		return username;
	}

	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getEmailAddress()
	{
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress)
	{
		this.emailAddress = emailAddress;
	}

	public String getCity()
	{
		return city;
	}

	public void setCity(String city)
	{
		this.city = city;
	}
}
