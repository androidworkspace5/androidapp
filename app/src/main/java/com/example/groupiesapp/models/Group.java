package com.example.groupiesapp.models;

import androidx.annotation.NonNull;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity
public class Group
{
	@PrimaryKey
	@NonNull
	private String groupName;

	public Group(@NonNull String groupName)
	{
		this.groupName = groupName;
	}

	public @NonNull
	String getGroupName()
	{
		return groupName;
	}

	public void setGroupName(@NonNull String groupName)
	{
		this.groupName = groupName;
	}
}
