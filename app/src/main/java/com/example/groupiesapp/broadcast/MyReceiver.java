package com.example.groupiesapp.broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;

import com.example.groupiesapp.R;

public class MyReceiver extends BroadcastReceiver
{
	@Override
	public void onReceive(Context context, Intent intent)
	{
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context, "test_notify")
				.setSmallIcon(R.drawable.ic_home)
				.setContentTitle("Test Notification")
				.setContentText("Hello! This is a test notification.")
				.setPriority(NotificationCompat.PRIORITY_DEFAULT);

		NotificationManagerCompat notificationManager = NotificationManagerCompat.from(context);

		notificationManager.notify(200, builder.build());
	}
}