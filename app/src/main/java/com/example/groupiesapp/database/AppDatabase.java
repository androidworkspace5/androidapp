package com.example.groupiesapp.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.groupiesapp.daos.GroupDAO;
import com.example.groupiesapp.models.Group;

@Database(entities = {Group.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase
{
	private static AppDatabase instance;

	public static AppDatabase getInstance(Context context)
	{
		if (instance == null)
		{
			instance = Room.databaseBuilder(context, AppDatabase.class, "groups-database").build();
		}

		return instance;
	}

	public abstract GroupDAO groupDAO();
}